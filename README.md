![Motigar](https://gitlab.com/Nikolas_erickson/motigars-mansion/-/raw/main/docs/zzz_CompanyLogo/motigar_bw.png)

# Motigar's Mansion

Welcome to "Motigar's Mansion," a spine-chilling horror game where players are ensnared within the sinister confines of a sprawling estate. Trapped amid a labyrinth of deadly traps and grotesque monsters, they must rely on their ingenuity and cunning to unravel the mysteries of the mansion and secure their escape. Armed with only scattered items and their own wit, players must navigate the treacherous corridors, solving intricate puzzles while evading the malevolent forces that haunt every shadow. With danger lurking around every corner, can you brave the horrors of Motigar's Mansion and emerge unscathed?



## Coming soon from Motigar Games
