# Group Documents

## Gantt stuff

[Document](https://vandalsuidaho-my.sharepoint.com/:x:/g/personal/vanc4342_vandals_uidaho_edu/EXf-eoFhPudGrpFmczcTgxoBvg-HDfAR20s9rmJnvfbkmA?e=1HrhJI)


## StoryBoard

[our storyboard](https://vandalsuidaho-my.sharepoint.com/:p:/g/personal/eric7862_vandals_uidaho_edu/EWRNIkk2avFHlupTqsiuawMBcs4thPcc3n0ihI7TJKXkIg?e=j3S8zf)

[Grading Key](https://webpages.uidaho.edu/drbc/cs383/assignments/04b_StoryboardMarkingKey.pdf)



## RFP

[Link to RFP](https://vandalsuidaho-my.sharepoint.com/:w:/g/personal/eric7862_vandals_uidaho_edu/EeII3C_zZFhHgi3F21lJGCYBAfKGW7YVeM7m0Ksa8Ehj7Q?e=7o39JD)

[Link to lecture notes](https://vandalsuidaho-my.sharepoint.com/personal/jbeeston_uidaho_edu/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fjbeeston%5Fuidaho%5Fedu%2FDocuments%2FCS383%2FSlides%2F02%5FRFP%2Epdf&parent=%2Fpersonal%2Fjbeeston%5Fuidaho%5Fedu%2FDocuments%2FCS383%2FSlides&ga=1)
